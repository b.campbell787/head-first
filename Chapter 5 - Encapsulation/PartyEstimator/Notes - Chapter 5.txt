// Encapsulation
// Documentation: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/access-modifiers

PUBLIC	-	The code is accessible by all classes
PRIVATE	-	The code is only accessible by within the same class, or instance of that class
PROTECTED - The code is accessible within the same class, or in a class that inherits that class

NO DECLARATION - By default, not declaring either public or private will set an instance variable to private

// Types
CONST - A constant type ensures the value cannot be changed
