﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PartyEstimator
{
    public partial class Form1 : Form
    {
        DinnerParty dinnerParty;

        public Form1()
        {
            InitializeComponent();

            dinnerParty = new DinnerParty();
            DisplayPartyDinnerCost();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void lbl_Cost_Click(object sender, EventArgs e)
        {
        }

        public void DisplayPartyDinnerCost()
        {
            lbl_Cost.Text = dinnerParty.CalculateCost(checkBox_healthyOption.Checked).ToString("c");
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dinnerParty.SetPartyOptions((int)numericUpDown1.Value, checkBox_Decorations.Checked);
            DisplayPartyDinnerCost();
        }

        private void checkBox_Decorations_CheckedChanged(object sender, EventArgs e)
        {
            dinnerParty.CalculateCostOfDecorations(checkBox_Decorations.Checked);
            this.checkBox_Decorations.Text = "Fancy Decorations";
            DisplayPartyDinnerCost();
        }

        private void checkBox_healthyOption_CheckedChanged(object sender, EventArgs e)
        {
            dinnerParty.CalculateCost(checkBox_healthyOption.Checked);
            this.checkBox_healthyOption.Text = "Healthy Option";
            DisplayPartyDinnerCost();

        }
    }
}
