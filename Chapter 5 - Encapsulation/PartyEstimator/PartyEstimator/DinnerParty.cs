﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartyEstimator
{
    class DinnerParty
    {

        public const int costOfFoodPerPerson = 25;
        private int numberOfPeople;
        decimal costOfDrinks;
        decimal costOfDecorations = 0;
        decimal applyDiscount = 0.95m;


        public void SetPartyOptions(int people, bool isNormalDecorations)
        {
            numberOfPeople = people;
            CalculateCostOfDecorations(isNormalDecorations);
        }

        public int GetNumberOfPeople()
        {
            return numberOfPeople;
        }

        public void SetHealthyOption(bool healthyOption)
        {
            costOfDrinks = healthyOption ? 5.00M : 20.00M;
        }


        public void CalculateCostOfDecorations(bool isFancy)
        {
            if (isFancy)
            {
                costOfDecorations = (numberOfPeople * 15) + 50;
            }
            else
            {
                costOfDecorations = (numberOfPeople * 7.5m) + 30;
            }

        }


        public decimal CalculateCost(bool isHealthyOption)
        {

            decimal totalCost = costOfDecorations + ((costOfDrinks + costOfFoodPerPerson) * numberOfPeople);

            if (isHealthyOption)
            {
                return totalCost * applyDiscount;
            }
            else
            {
                return totalCost;
            }

        }



    }
}
