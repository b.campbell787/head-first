﻿namespace PartyEstimator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.checkBox_Decorations = new System.Windows.Forms.CheckBox();
            this.checkBox_healthyOption = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_Cost = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number of People";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(4, 26);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(106, 20);
            this.numericUpDown1.TabIndex = 1;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // checkBox_Decorations
            // 
            this.checkBox_Decorations.AutoSize = true;
            this.checkBox_Decorations.Location = new System.Drawing.Point(7, 58);
            this.checkBox_Decorations.Name = "checkBox_Decorations";
            this.checkBox_Decorations.Size = new System.Drawing.Size(115, 17);
            this.checkBox_Decorations.TabIndex = 2;
            this.checkBox_Decorations.Text = "Fancy Decorations";
            this.checkBox_Decorations.UseVisualStyleBackColor = true;
            this.checkBox_Decorations.CheckedChanged += new System.EventHandler(this.checkBox_Decorations_CheckedChanged);
            // 
            // checkBox_healthyOption
            // 
            this.checkBox_healthyOption.AutoSize = true;
            this.checkBox_healthyOption.Location = new System.Drawing.Point(7, 81);
            this.checkBox_healthyOption.Name = "checkBox_healthyOption";
            this.checkBox_healthyOption.Size = new System.Drawing.Size(96, 17);
            this.checkBox_healthyOption.TabIndex = 3;
            this.checkBox_healthyOption.Text = "Healthy Option";
            this.checkBox_healthyOption.UseVisualStyleBackColor = true;
            this.checkBox_healthyOption.CheckedChanged += new System.EventHandler(this.checkBox_healthyOption_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cost";
            // 
            // lbl_Cost
            // 
            this.lbl_Cost.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_Cost.Location = new System.Drawing.Point(42, 113);
            this.lbl_Cost.Name = "lbl_Cost";
            this.lbl_Cost.Size = new System.Drawing.Size(94, 32);
            this.lbl_Cost.TabIndex = 5;
            this.lbl_Cost.Click += new System.EventHandler(this.lbl_Cost_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(165, 156);
            this.Controls.Add(this.lbl_Cost);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBox_healthyOption);
            this.Controls.Add(this.checkBox_Decorations);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.CheckBox checkBox_Decorations;
        private System.Windows.Forms.CheckBox checkBox_healthyOption;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_Cost;
    }
}

