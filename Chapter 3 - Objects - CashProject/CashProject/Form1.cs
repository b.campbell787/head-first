﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashProject
{
    public partial class Form1 : Form
    {

        Guy joe; //Declared here, initialised later - required for scope access throughout class
        Guy bob; //Object declarations work the same as variable declarations
        int bank = 100;

        public Form1()
        {
            InitializeComponent();

            joe = new Guy(); // Method one of intialisation
            joe.Name = "joe";
            joe.Cash = 50;

            bob = new Guy() { Name = "Bob", Cash = 100 }; //Method two of initialisation

            UpdateForm();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (bank >= 10)
            {
                bank -= joe.ReceiveCash(10);
                UpdateForm();
            }
            else
            {
                MessageBox.Show("The bank is out of money");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bank += bob.GiveCash(5);
            UpdateForm();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public void UpdateForm()
        {
            joesCashLabel.Text = joe.Name + " has $" + joe.Cash;
            bobsCashLabel.Text = bob.Name + " has $" + bob.Cash;
            bankCashLabel.Text = "The bank has $" + bank;
        }


        private void joeGivesToBob_Click(object sender, EventArgs e)
        {
            joe.GiveCash(bob.ReceiveCash(10));
            UpdateForm();
        }

        private void bobGivesToJoe_Click(object sender, EventArgs e)
        {
            bob.GiveCash(joe.ReceiveCash(5));
            UpdateForm();
        }
    }
}
