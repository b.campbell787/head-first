﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Variables
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //This is a comment
            string name = "Quentin";
            int x = 3;
            x = x * 17;
            double d = Math.PI / 2;

            MessageBox.Show("name is: " + name + "\nx is: " + x + "\nd is: " + d);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int x = 5;

            if (x == 10)
            {
                MessageBox.Show("X is 10");
            }
            else
            {
                MessageBox.Show("X is not 10");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            int someValue = 4;

            string name = "Bobbo Jr";

            if ((someValue == 3) && (name == "Joe"))
            {
                MessageBox.Show("x is 3 and the name is Joe");
            }
            MessageBox.Show("This line runes no matter what");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int count = 0;

            while (count < 10)
            {
                count = count + 1;
            }

            for ( int i = 0; i < 5; i++)
            {
                count = count - 1;
            }

            MessageBox.Show("The answer is: " + count);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            int result = 0;

            int x = 6;

            while (x > 3)
            {
                result = result + x;

                x = x - 1;
            }

            for (int z = 1; z < 3; z++)
            {
                result = result + z;
            }

            MessageBox.Show("The result is: " + result);
        }

        private void button6_Click(object sender, EventArgs e)
        {

            int x = 3;

            string result = "";

            if(x > 2)
            {
                result += "a";

                while (x > 0)
                {
                    x = x - 1;
                    result = result += "-";

                    if (x == 2)
                    {
                        result = result + "b c";
                    }

                    if (x == 1)
                    {
                        result = result + "d";
                        x = x - 1;
                    }
                }

            }
            MessageBox.Show(result);           

        }
    }
}
