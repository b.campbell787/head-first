﻿namespace MileageCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.lbl_StartingMileage = new System.Windows.Forms.Label();
            this.lbl_EndingMileage = new System.Windows.Forms.Label();
            this.lbl_Amount = new System.Windows.Forms.Label();
            this.lbl_AmountOwed = new System.Windows.Forms.Label();
            this.btn_Calculate = new System.Windows.Forms.Button();
            this.btn_DisplayMiles = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(126, 37);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(150, 20);
            this.numericUpDown1.TabIndex = 0;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(126, 77);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(150, 20);
            this.numericUpDown2.TabIndex = 1;
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // lbl_StartingMileage
            // 
            this.lbl_StartingMileage.AutoSize = true;
            this.lbl_StartingMileage.Location = new System.Drawing.Point(18, 39);
            this.lbl_StartingMileage.Name = "lbl_StartingMileage";
            this.lbl_StartingMileage.Size = new System.Drawing.Size(83, 13);
            this.lbl_StartingMileage.TabIndex = 2;
            this.lbl_StartingMileage.Text = "Starting Mileage";
            // 
            // lbl_EndingMileage
            // 
            this.lbl_EndingMileage.AutoSize = true;
            this.lbl_EndingMileage.Location = new System.Drawing.Point(21, 79);
            this.lbl_EndingMileage.Name = "lbl_EndingMileage";
            this.lbl_EndingMileage.Size = new System.Drawing.Size(80, 13);
            this.lbl_EndingMileage.TabIndex = 3;
            this.lbl_EndingMileage.Text = "Ending Mileage";
            // 
            // lbl_Amount
            // 
            this.lbl_Amount.AutoSize = true;
            this.lbl_Amount.Location = new System.Drawing.Point(27, 114);
            this.lbl_Amount.Name = "lbl_Amount";
            this.lbl_Amount.Size = new System.Drawing.Size(74, 13);
            this.lbl_Amount.TabIndex = 4;
            this.lbl_Amount.Text = "Amount Owed";
            this.lbl_Amount.Click += new System.EventHandler(this.lbl_Amount_Click);
            // 
            // lbl_AmountOwed
            // 
            this.lbl_AmountOwed.AutoSize = true;
            this.lbl_AmountOwed.Location = new System.Drawing.Point(123, 114);
            this.lbl_AmountOwed.Name = "lbl_AmountOwed";
            this.lbl_AmountOwed.Size = new System.Drawing.Size(0, 13);
            this.lbl_AmountOwed.TabIndex = 5;
            // 
            // btn_Calculate
            // 
            this.btn_Calculate.Location = new System.Drawing.Point(95, 140);
            this.btn_Calculate.Name = "btn_Calculate";
            this.btn_Calculate.Size = new System.Drawing.Size(78, 30);
            this.btn_Calculate.TabIndex = 6;
            this.btn_Calculate.Text = "Calculate";
            this.btn_Calculate.UseVisualStyleBackColor = true;
            this.btn_Calculate.Click += new System.EventHandler(this.btn_Calculate_Click);
            // 
            // btn_DisplayMiles
            // 
            this.btn_DisplayMiles.Location = new System.Drawing.Point(198, 141);
            this.btn_DisplayMiles.Name = "btn_DisplayMiles";
            this.btn_DisplayMiles.Size = new System.Drawing.Size(101, 29);
            this.btn_DisplayMiles.TabIndex = 7;
            this.btn_DisplayMiles.Text = "Display Miles";
            this.btn_DisplayMiles.UseVisualStyleBackColor = true;
            this.btn_DisplayMiles.Click += new System.EventHandler(this.btn_DisplayMiles_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 183);
            this.Controls.Add(this.btn_DisplayMiles);
            this.Controls.Add(this.btn_Calculate);
            this.Controls.Add(this.lbl_AmountOwed);
            this.Controls.Add(this.lbl_Amount);
            this.Controls.Add(this.lbl_EndingMileage);
            this.Controls.Add(this.lbl_StartingMileage);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.numericUpDown1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label lbl_StartingMileage;
        private System.Windows.Forms.Label lbl_EndingMileage;
        private System.Windows.Forms.Label lbl_Amount;
        private System.Windows.Forms.Label lbl_AmountOwed;
        private System.Windows.Forms.Button btn_Calculate;
        private System.Windows.Forms.Button btn_DisplayMiles;
    }
}

