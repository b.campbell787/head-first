﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MileageCalculator
{
    public partial class Form1 : Form
    {

        private int startingMileage = 0;
        private int endingMileage = 0;

        private double milesTraveled;
        private double reimburseRate = 0.39d;
        private double amountOwed;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "Mileage Calculator"; //set the title
        }

        private void btn_Calculate_Click(object sender, EventArgs e)
        {

            startingMileage = (int) numericUpDown1.Value;
            endingMileage = (int)numericUpDown2.Value;

            CalculateValues();

            //validate the values so ending is higher than starting
            if (numericUpDown1.Value >= numericUpDown2.Value)
            {
                MessageBox.Show("The starting mileage must " +
                                "be less than the ending " +
                                "mileage.", "Cannot Calculate");

            }

        }

        private void lbl_Amount_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        public void CalculateValues()
        {
            milesTraveled = endingMileage -= startingMileage;
            amountOwed = milesTraveled * reimburseRate;
            lbl_AmountOwed.Text = "$ " + amountOwed;
        }

        private void btn_DisplayMiles_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Miles: " + milesTraveled, "Miles Traveled");
        }
    }
}
