﻿namespace Exercise__S_wap
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Lloyd = new System.Windows.Forms.Button();
            this.btn_Lucinda = new System.Windows.Forms.Button();
            this.btn_Swap = new System.Windows.Forms.Button();
            this.btn_AlterObject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Lloyd
            // 
            this.btn_Lloyd.Location = new System.Drawing.Point(62, 26);
            this.btn_Lloyd.Name = "btn_Lloyd";
            this.btn_Lloyd.Size = new System.Drawing.Size(97, 35);
            this.btn_Lloyd.TabIndex = 0;
            this.btn_Lloyd.Text = "Lloyd";
            this.btn_Lloyd.UseVisualStyleBackColor = true;
            this.btn_Lloyd.Click += new System.EventHandler(this.btn_Lloyd_Click);
            // 
            // btn_Lucinda
            // 
            this.btn_Lucinda.Location = new System.Drawing.Point(62, 67);
            this.btn_Lucinda.Name = "btn_Lucinda";
            this.btn_Lucinda.Size = new System.Drawing.Size(97, 35);
            this.btn_Lucinda.TabIndex = 1;
            this.btn_Lucinda.Text = "Lucinda";
            this.btn_Lucinda.UseVisualStyleBackColor = true;
            this.btn_Lucinda.Click += new System.EventHandler(this.btn_Lucinda_Click);
            // 
            // btn_Swap
            // 
            this.btn_Swap.Location = new System.Drawing.Point(63, 119);
            this.btn_Swap.Name = "btn_Swap";
            this.btn_Swap.Size = new System.Drawing.Size(98, 29);
            this.btn_Swap.TabIndex = 2;
            this.btn_Swap.Text = "Swap";
            this.btn_Swap.UseVisualStyleBackColor = true;
            this.btn_Swap.Click += new System.EventHandler(this.btn_Swap_Click);
            // 
            // btn_AlterObject
            // 
            this.btn_AlterObject.Location = new System.Drawing.Point(63, 154);
            this.btn_AlterObject.Name = "btn_AlterObject";
            this.btn_AlterObject.Size = new System.Drawing.Size(98, 28);
            this.btn_AlterObject.TabIndex = 3;
            this.btn_AlterObject.Text = "Alter object";
            this.btn_AlterObject.UseVisualStyleBackColor = true;
            this.btn_AlterObject.Click += new System.EventHandler(this.btn_AlterObject_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(222, 198);
            this.Controls.Add(this.btn_AlterObject);
            this.Controls.Add(this.btn_Swap);
            this.Controls.Add(this.btn_Lucinda);
            this.Controls.Add(this.btn_Lloyd);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Lloyd;
        private System.Windows.Forms.Button btn_Lucinda;
        private System.Windows.Forms.Button btn_Swap;
        private System.Windows.Forms.Button btn_AlterObject;
    }
}

