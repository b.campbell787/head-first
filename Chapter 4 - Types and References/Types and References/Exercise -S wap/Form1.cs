﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise__S_wap
{
    public partial class Form1 : Form
    {
        Elephant lucinda;
        Elephant lloyd;
        Elephant swapObject;

        public Form1()
        {
            InitializeComponent();

            lucinda = new Elephant() { Name = "Lucinda", EarSize = 33 };
            lloyd = new Elephant() { Name = "Lloyd", EarSize = 40 };

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "Swap";
        }

        private void btn_Lloyd_Click(object sender, EventArgs e)
        {
            lloyd.WhoAmI();
        }

        private void btn_Lucinda_Click(object sender, EventArgs e)
        {
            lucinda.WhoAmI();
        }

        private void btn_Swap_Click(object sender, EventArgs e)
        {
            lloyd.TellMe("Hi", lucinda);

            /*
             * When SpeakTo method is called it uses its first param (a reference to lucinda) to 
             * call Lucinda's TellMe() method
             */
            lloyd.SpeakTo(lucinda, "Hello");


            /*
             * Swap Object always equals value stored in lloyd
             * 1st swap lloyd = lucinda and lucinda = lloyd
             * 2nd swap it returns as now swapObject = the value in lloyd which is lucinda 
             * On 2nd swap, the values return to original state
             * 
             * Note there's no new statement for swapObject as we don't want to create
             * another instance of Elephant
             */

            swapObject = lloyd; 
            lloyd = lucinda;
            lucinda = swapObject;

            MessageBox.Show("Objects swapped");

        }

        private void btn_AlterObject_Click(object sender, EventArgs e)
        {
            lloyd = lucinda;
            lloyd.EarSize = 1111;
            lloyd.WhoAmI();
        }
    }
}
