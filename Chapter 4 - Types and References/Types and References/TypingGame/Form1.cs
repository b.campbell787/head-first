﻿using System;
using System.Windows.Forms;

namespace TypingGame
{
    public partial class Form1 : Form
    {

        Random random = new Random();
        Stats stats = new Stats();

        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Setup the form so its size is set and cannot be altered
            this.Text = "Hit the Keys";
            this.MinimizeBox = false;
            this.MaximizeBox = false;

            // Set the timer controls
            this.timer1.Enabled = true;
            timer1.Interval = 800; 

            // Required for key presses to be consumed
            this.KeyPreview = true;


        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            //Add a random key to the listbox
            listBox1.Items.Add((Keys)random.Next(65, 90));

            if (listBox1.Items.Count > 7)
            {
                listBox1.Items.Clear();
                listBox1.Items.Add("Game Over!");
                timer1.Stop();
            }
        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            // This gets called each time a key is pressed

            // Increases the difficulty as the player gets more correct
            if (listBox1.Items.Contains(e.KeyCode))
            {
                listBox1.Items.Remove(e.KeyCode);
                listBox1.Refresh();
                if (timer1.Interval > 400)
                    timer1.Interval -= 10;

                if (timer1.Interval > 250)
                    timer1.Interval -= 7;

                if (timer1.Interval > 100)
                    timer1.Interval -= 2;

                progBarDifficulty.Value = 800 - timer1.Interval;

                // The user pressed a correct key, so stats object must again be updated
                stats.Update(true);
            }
            else
            {
                // The user pressed an incorrect key, so stats object must again be updated
                stats.Update(false);
            }

            // Update the lables on the status strip
            lblCorrect.Text = $"Correct: {stats.Correct}";
            lblMissed.Text = $"Missed: {stats.Missed}";
            lblTotal.Text = $"Total: {stats.Total}";
            lblAccuracy.Text = $"Accuracy: {stats.Accuracy}%";

        }

    }
}
