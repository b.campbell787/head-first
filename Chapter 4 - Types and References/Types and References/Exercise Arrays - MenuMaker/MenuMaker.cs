﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_Arrays___MenuMaker
{

    class MenuMaker
    {
        public Random randomizer;          //create Random object

        //Create arrays of ingredients
        string[] Meats = { "beef", "pastrami", "salami", "turkey", "ham" };
        string[] Condiments = { "honey", "mayo", "syrup", "garlic dip", "sweet chilli" };
        string[] Breads = { "rye", "white", "foccacia", "baguette" };


        /*
         * Get a random item from each array within the array bounds
         */
        public string GetMenuItem()
        {
            string randomMeat = Meats[randomizer.Next(Meats.Length)];
            string randomCondiment = Condiments[randomizer.Next(Condiments.Length)];
            string randomBread = Breads[randomizer.Next(Breads.Length)];

            return $"{randomMeat} with {randomCondiment} on {randomBread}";
        }


    }
}
