﻿namespace Exercise_Arrays___MenuMaker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblResultSix = new System.Windows.Forms.Label();
            this.lblResultFive = new System.Windows.Forms.Label();
            this.lblResultFour = new System.Windows.Forms.Label();
            this.lblResultThree = new System.Windows.Forms.Label();
            this.lblResultTwo = new System.Windows.Forms.Label();
            this.lblResultOne = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Meal One";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Meal Two";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Meal Three";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Meal Four";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(49, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Meal Five";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Meal Six";
            // 
            // lblResultSix
            // 
            this.lblResultSix.AutoSize = true;
            this.lblResultSix.Location = new System.Drawing.Point(156, 191);
            this.lblResultSix.Name = "lblResultSix";
            this.lblResultSix.Size = new System.Drawing.Size(68, 13);
            this.lblResultSix.TabIndex = 11;
            this.lblResultSix.Text = "Meal Results";
            // 
            // lblResultFive
            // 
            this.lblResultFive.AutoSize = true;
            this.lblResultFive.Location = new System.Drawing.Point(156, 160);
            this.lblResultFive.Name = "lblResultFive";
            this.lblResultFive.Size = new System.Drawing.Size(68, 13);
            this.lblResultFive.TabIndex = 10;
            this.lblResultFive.Text = "Meal Results";
            // 
            // lblResultFour
            // 
            this.lblResultFour.AutoSize = true;
            this.lblResultFour.Location = new System.Drawing.Point(156, 128);
            this.lblResultFour.Name = "lblResultFour";
            this.lblResultFour.Size = new System.Drawing.Size(68, 13);
            this.lblResultFour.TabIndex = 9;
            this.lblResultFour.Text = "Meal Results";
            // 
            // lblResultThree
            // 
            this.lblResultThree.AutoSize = true;
            this.lblResultThree.Location = new System.Drawing.Point(156, 99);
            this.lblResultThree.Name = "lblResultThree";
            this.lblResultThree.Size = new System.Drawing.Size(68, 13);
            this.lblResultThree.TabIndex = 8;
            this.lblResultThree.Text = "Meal Results";
            // 
            // lblResultTwo
            // 
            this.lblResultTwo.AutoSize = true;
            this.lblResultTwo.Location = new System.Drawing.Point(156, 71);
            this.lblResultTwo.Name = "lblResultTwo";
            this.lblResultTwo.Size = new System.Drawing.Size(68, 13);
            this.lblResultTwo.TabIndex = 7;
            this.lblResultTwo.Text = "Meal Results";
            // 
            // lblResultOne
            // 
            this.lblResultOne.AutoSize = true;
            this.lblResultOne.Location = new System.Drawing.Point(156, 39);
            this.lblResultOne.Name = "lblResultOne";
            this.lblResultOne.Size = new System.Drawing.Size(68, 13);
            this.lblResultOne.TabIndex = 6;
            this.lblResultOne.Text = "Meal Results";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 236);
            this.Controls.Add(this.lblResultSix);
            this.Controls.Add(this.lblResultFive);
            this.Controls.Add(this.lblResultFour);
            this.Controls.Add(this.lblResultThree);
            this.Controls.Add(this.lblResultTwo);
            this.Controls.Add(this.lblResultOne);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblResultSix;
        private System.Windows.Forms.Label lblResultFive;
        private System.Windows.Forms.Label lblResultFour;
        private System.Windows.Forms.Label lblResultThree;
        private System.Windows.Forms.Label lblResultTwo;
        private System.Windows.Forms.Label lblResultOne;
    }
}

