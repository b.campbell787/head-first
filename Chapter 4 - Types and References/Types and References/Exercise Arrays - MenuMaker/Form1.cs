﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_Arrays___MenuMaker
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();

            /*
             * Create reference variable for menuMaker class
             * Ensure that randomizer object is initialized
             */ 
            MenuMaker menuMaker = new MenuMaker() { randomizer = new Random() };

            lblResultOne.Text = menuMaker.GetMenuItem();
            lblResultTwo.Text = menuMaker.GetMenuItem();
            lblResultThree.Text = menuMaker.GetMenuItem();
            lblResultFour.Text = menuMaker.GetMenuItem();
            lblResultFive.Text = menuMaker.GetMenuItem();
            lblResultSix.Text = menuMaker.GetMenuItem();
        }

    }
}
