﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{

    class MethodTester
    {

        ///
        /// Built in Types
        ///
        int myInt = 15;
        double myDouble = 14.5F;   //suffix of M or F makes it M: Decimal or F: Float
        string myString = "Hello World";
        char myChar = 'x';

        ///
        /// Escape Sequences for Strings
        ///
        string newLine = "\n This will be on the second line \n This will be on the third";
        string tabLine = "\t This will be tabbed out";


        public void CastableValues()
        {
            int myInt = 10;
            byte myByte = (byte)myInt;
            double myDouble = (double)myByte;

            //bool myBool = (bool) myDouble;       //cannot cast double to bool
            string myString = "false";
            //myBool = (bool) myString;            //cannot cast string to bool

            //myString = (string) myInt;           //cannot cast int to string
            myString = myInt.ToString();

            //myBool = (bool) myByte;              //cannot cast byte to bool
            //myByte = (byte) myBool;              //cannot cast bool to byte

            short myShort = (short)myInt;
            char myChar = 'x';
            //myString = (string) myChar;          //cannot cast char to a string
            long myLong = (long)myInt;
            decimal myDecimal = (decimal)myLong;
            myString = myString + myInt + myByte
            + myDouble + myChar;
        }

        public int BoolMethod(bool yesNo)
        {
            if (yesNo)
            {
                return 45;
            }
            else
            {
                return 0;
            }
        }

        public void CreateArray()
        {
            int[] heights;                  //declare array

            heights = new int[15];          //new keyword required as array is an object
                                            //this array has 15 elements in it

            heights[0] = 10;
            heights[1] = 45;
            heights[2] = 87;
            heights[3] = 26;                //set value of 4th element, as first element is heights[0]

            Console.WriteLine("Value in element 1 is: ", heights[1]);   //acces the value in the same way as a variable
        }

        public void CreateCleanerArray()
        {
            int[] heights = new int[] { 34, 15, 67, 44, 78 };  //declare and initialize the array, this is a 'dynamic array'

            foreach (var value in heights)
            {
                Console.WriteLine($"Value is: {value}");
            }

        }

        public void PrintArrayWithIndex()
        {
            int[] heights = new int[] { 34, 15, 67, 44, 78 };  //declare and initialize the array, this is a 'dynamic array'

            for (int i = 0; i < heights.Length; i++)
            {
                Console.WriteLine($"The value in heights index {i} is {heights.GetValue(i)}");
            }
        }


        ///
        /// Sharpen your pencil
        ///     PAGES:  157
        ///

        public void BiggestEars()
        {
            Elephant[] elephants = new Elephant[7];
            elephants[0] = new Elephant() { Name = "Lloyd", EarSize = 40 };
            elephants[1] = new Elephant() { Name = "Lucinda", EarSize = 33 };
            elephants[2] = new Elephant() { Name = "Larry", EarSize = 42 };
            elephants[3] = new Elephant() { Name = "Lucille", EarSize = 32 };
            elephants[4] = new Elephant() { Name = "Lars", EarSize = 44 };
            elephants[5] = new Elephant() { Name = "Linda", EarSize = 37 };
            elephants[6] = new Elephant() { Name = "Humphrey", EarSize = 45 };

            Elephant biggestEars = elephants[0];            //sets the value of biggestEars initially to 40
            
            for (int i = 1; i < elephants.Length; i++)      //starts on the second element so the first isn't compared against itself
            {
                if (elephants[i].EarSize > biggestEars.EarSize) //compares current index value to existing biggestEars
                {
                    biggestEars = elephants[i];             //sets biggestEars based on condition
                }
                Console.WriteLine("Currently comparing: " + elephants[i].EarSize + " vs " + biggestEars.EarSize);   //debugging trail
            }
            Console.WriteLine("\nThe biggest ears are size: " + biggestEars.EarSize.ToString());    //result
        }

        ///
        /// Sharpen your pencil
        ///     PAGES:  158
        ///

        public void CodeMagnets()
        {
            int y = 0;
            string result = "";

            string[] islands = new string[4];
            islands[0] = "Bermuda";
            islands[1] = "Fiji";
            islands[2] = "Azores";
            islands[3] = "Cozumel";

            int[] index = new int[4];
            index[0] = 1;
            index[1] = 3;
            index[2] = 0;
            index[3] = 2;

            int refNum;
            while (y < 4)
            {
                refNum = index[y];
                result += "\nisland = ";
                result += islands[refNum];
                y = y + 1;  //y++
            }
                Console.WriteLine(result);
        }

        //Required for BiggestEars method
        internal class Elephant
        {
            public string Name;
            public int EarSize;
        }

                     
    }
}
