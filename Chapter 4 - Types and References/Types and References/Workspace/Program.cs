﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{

    class Program
    {


        static void Main(string[] args)
        {
            MethodTester methods;
            methods = new MethodTester();


            methods.CastableValues();
            Console.WriteLine("\n---------- My Bool ---------- ");
            Console.WriteLine(methods.BoolMethod(true));


            Console.WriteLine("\n---------- My Arrays ---------- ");
            methods.CreateCleanerArray();

            Console.WriteLine("\n---------- My Array with Index ---------- ");
            methods.PrintArrayWithIndex();


            Console.WriteLine("\n---------- Loop elephant array ---------- ");
            methods.BiggestEars();

            Console.WriteLine("\n---------- Code Magnets page: 158 ---------- ");
            methods.CodeMagnets();

            Console.ReadLine();

        }


    }

}
