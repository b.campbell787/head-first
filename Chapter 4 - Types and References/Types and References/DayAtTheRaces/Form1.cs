﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayAtTheRaces
{
    public partial class lbl_MinimumBet : Form
    {

        Greyhound[] greyhounds = new Greyhound[3];
        Guy[] guys = new Guy[3];

        public Form()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void SetupRaceTracks()
        {
            guys[0] = new Guy("Joe", null, 50, joeButton, joeBet);

        }

        private void joeButton_CheckedChanged(object sender, EventArgs e)
        {
            this.Text = "Joe";
        }

        private void bobButton_CheckedChanged(object sender, EventArgs e)
        {
            this.Text = "Bob";
        }

        private void alButton_CheckedChanged(object sender, EventArgs e)
        {
            this.Text = "Al";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
