﻿namespace DayAtTheRaces
{
    partial class lbl_MinimumBet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.joeButton = new System.Windows.Forms.RadioButton();
            this.bobButton = new System.Windows.Forms.RadioButton();
            this.alButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.alButton);
            this.groupBox1.Controls.Add(this.bobButton);
            this.groupBox1.Controls.Add(this.joeButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 252);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 186);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Betting Parlour";
            // 
            // joeButton
            // 
            this.joeButton.AutoSize = true;
            this.joeButton.Location = new System.Drawing.Point(18, 43);
            this.joeButton.Name = "joeButton";
            this.joeButton.Size = new System.Drawing.Size(85, 17);
            this.joeButton.TabIndex = 1;
            this.joeButton.TabStop = true;
            this.joeButton.Text = "radioButton1";
            this.joeButton.UseVisualStyleBackColor = true;
            this.joeButton.CheckedChanged += new System.EventHandler(this.joeButton_CheckedChanged);
            // 
            // bobButton
            // 
            this.bobButton.AutoSize = true;
            this.bobButton.Location = new System.Drawing.Point(18, 77);
            this.bobButton.Name = "bobButton";
            this.bobButton.Size = new System.Drawing.Size(85, 17);
            this.bobButton.TabIndex = 2;
            this.bobButton.TabStop = true;
            this.bobButton.Text = "radioButton2";
            this.bobButton.UseVisualStyleBackColor = true;
            this.bobButton.CheckedChanged += new System.EventHandler(this.bobButton_CheckedChanged);
            // 
            // alButton
            // 
            this.alButton.AutoSize = true;
            this.alButton.Location = new System.Drawing.Point(18, 110);
            this.alButton.Name = "alButton";
            this.alButton.Size = new System.Drawing.Size(85, 17);
            this.alButton.TabIndex = 3;
            this.alButton.TabStop = true;
            this.alButton.Text = "radioButton3";
            this.alButton.UseVisualStyleBackColor = true;
            this.alButton.CheckedChanged += new System.EventHandler(this.alButton_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Minimum Bet";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbl_MinimumBet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Name = "lbl_MinimumBet";
            this.Text = "Minimum Bet";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton joeButton;
        private System.Windows.Forms.RadioButton bobButton;
        private System.Windows.Forms.RadioButton alButton;
        private System.Windows.Forms.Label label1;
    }
}

