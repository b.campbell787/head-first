﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayAtTheRaces
{
    class Greyhound
    {

        public double StartingPostion;
        public double RacetrackLength;
        public PictureBox MyPictureBox;
        public int Location = 0;
        public Random Randomizer;

        public void Run()
        {
            ///move foward either 1-4 spaces at random
            ///update the postition of MyPictureBox
            ///return true if I won the race
            
            int moveSteps = Randomizer.Next(1,4);
            Location += moveSteps;
            MyPictureBox.Update();
        }
        public void TakeStartingPosition()
        {
            ///reset my location to the start line
            StartingPostion = 0;

        }

    }
}
