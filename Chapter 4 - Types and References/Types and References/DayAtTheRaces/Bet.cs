﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayAtTheRaces
{
    class Bet
    {
        public int Amount;
        public Greyhound Dog;
        public Guy Bettor;


        public Bet(int amount, Greyhound dog, Guy bettor)
        {
            this.Amount = amount;
            this.Dog = dog;
            this.Bettor = bettor;
        }

        public string GetDescription()
        {
            // return a string that says who placed the bet
            // how much cash was the bet
            // what dog was bet on ("joe bets 8 on dog #4")
            // if amount is zero -- no bet can be placed
            
            return "";
        }

        public int PayOut(int Winner)
        {
            //the parameter is the winner of the race
            // if the dog won return the amount bet
            // otherwise  return the negative of the bet
            return 0;

        }
    }
}
