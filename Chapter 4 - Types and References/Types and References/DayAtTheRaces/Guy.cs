﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DayAtTheRaces
{
    class Guy
    {
        public string Name; // the guys name
        public Bet MyBet; //instance of Bet
        public int Cash; //how much cash he has

        //GUI controls
        public RadioButton MyRadioButton;
        public Label MyLabel;

        public Guy(string name, Bet myBet, int cash, RadioButton radioButton, Label label)
        {
            this.Name = name;
            this.MyBet = myBet;
            this.Cash = cash;
            this.MyRadioButton = radioButton;
            this.MyLabel = label;
        }

        public void UpdateLabels()
        {

            if (MyLabel.Text == null)
            {

            }
            //Set my label to my bets description, and the label on my 
            //radio button to show my cash ("Joe has 40 bucks")
            MyLabel.Update();
        }

        public void ClearBet()
        {
            //Reset my bet to zero
        }

        public bool PlaceBet(int Amount, int Dog)
        {
            //place a new bet and store it in my bet field
            //return true if the guy had enough money to bet
            // bets are represented by instances of Bet

            return false;
        }

        public void Collect(int Winner)
        {
            //Ask my bet to pay out - use the bet object
        }




    }

}
